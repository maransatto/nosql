// 1. Total de Documentos Existentes
db.restaurants.count();

// 2. Exibir todos os documentos da coleção restaurants
db.restaurants.find();

// 3. Exibir todos os documentos na coleção restaurants formatados corretamente
db.restaurants.find().pretty();

// 4. Exibir os campos restaurant_id, name, borough e cuisine para todos os documentos
db.restaurants.find().select("restaurant_id name borough cuisine");

// 5. Exibir os campos restaurant_id, name, borough e cuisine para todos os documentos mas sem exibir o campo _id
db.restaurants.find({}, { _id: 0 }).select("restaurant_id name borough cuisine");

// 6. Exibir todos os restaurantes cuja localização é o bairro (borough) do Bron
db.restaurants.find({ borough: 'Bronx' });

// 7. Exibir apenas os cinco primeiros restaurantes cuja localização é o bairro (borough) do Bronx
db.restaurants.find({ borough: 'Bronx' }).limit(5);

db.restaurants.find();

// 8. Consultar todos os restaurantes que receberam nota (score) superior a 90.
db.restaurants.aggregate({$match: {"grades.score": {$gt:90}}});

// 9. Consultar todos os restaurantes que receberam nota (score) superior ou igual a 80 mas menor que 100.
db.restaurants.aggregate({$match: {"grades.score": {$gte:80, $lt: 100}}});

// 10. Consulte os restaurantes localizados em uma latitude inferior a -95.754168
db.restaurants.find({'address.coord.1' : { $lt: -95.754168 }});
// o segundo índice é latitude, na verdade, os restaurantes são todos nos EUA, ou seja, tudo latidude positiva.
// por isso não retornou nada

// 11. Escreva uma consulta para encontrar os restaurants cuja culinária (cuisine) não é do American, que conseguiram nota superior a 70 e que estão localizados em uma longitude inferior a -65.754168.
db.restaurants.find({cuisine: { $ne: 'American' }, "grades.score": {$gt:70}, 'address.coord.0' : { $lt: -65.754168 }});

// 12. Exibe o _id, nome, bairro e cozinha de todos os restaurantes com nome iniciado com ‘Wil’
db.restaurants.find({name: /^Wil/},{ _id: 1, name: 1, borough : 1, cuisine: 1 });

// 13. Exibe o _id, nome, bairro e cozinha de todos os restaurantes com nome contendo‘Reg’
db.restaurants.find({name: /Reg/},{ _id: 1, name: 1, borough : 1, cuisine: 1 });

// 14. Exiba o id, nome, bairro e cozinha dos restaurants localizados nos bairros Staten Island, Queens ou Bronx.
db.restaurants.find({borough: {$in : ['Staten Island', 'Queens', 'Bronx'] }},{ _id: 1, name: 1, borough : 1, cuisine: 1 });

// 15. Faça uma consulta para exibir o id, nome e classificação (grade) para restaurantes com classificação (grade) A e nota (score) 11 especificamente na data padrão ISO 2014-08-11T00:00:00Z.
db.restaurants.find({ $and: [{'grades.grade': 'A', 'grades.score' : 11}] },{ _id: 1, name: 1, grades : 1 });

// 16. Faça uma consulta que mostre os nomes e bairros de todos os restaurantes, em ordem crescente de nome.
db.restaurants.find({}, {name: 1, borough: 1}).sort('name');