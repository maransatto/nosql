db.restaurants.insertOne({
    address: {
        building: '338',
        coord: [-46.695855, -23.618330],
        street: 'Rua Baltazar Fernandes',
        zipcode: '04583-020'
    },
    borough: 'Vila Cordeiro',
    cuisine: 'Churrasco',
    grades: [{
        date : ISODate('2019-03-18T22:00:00.000-03:00'),
        grade: 'A',
        score: 50
    },{
        date : ISODate('2019-03-17T22:00:00.000-03:00'),
        grade: 'B',
        score: 7
    }],
    name: 'Recanto Gaúcho'
});

db.restaurants.insertMany([{
    address: {
        building: '102',
        coord: [-46.696240, -23.618485 ],
        street: 'Avenida Jurubatuba',
        zipcode: '04583-100'
    },
    borough: 'Vila Cordeiro',
    cuisine: 'Brasileira',
    grades: [{
        date : ISODate('2019-03-18T22:00:00.000-03:00'),
        grade: 'C',
        score: 9
    },{
        date : ISODate('2019-03-17T22:00:00.000-03:00'),
        grade: 'D',
        score: 1
    }],
    name: 'Varanda Jurubatuba'
},{
    address: {
        building: '1089',
        coord: [-46.698667, -23.624125],
        street: 'Avenida Roque Petroni Júnior',
        zipcode: '04707-120'
    },
    borough: 'Chácara Santo Antônio',
    cuisine: 'Japonesa',
    grades: [{
        date : ISODate('2019-03-18T22:00:00.000-03:00'),
        grade: 'A',
        score: 100
    },{
        date : ISODate('2019-03-17T22:00:00.000-03:00'),
        grade: 'A',
        score: 98
    }],
    name: 'Gendai'
},{
    address: {
        building: '285',
        coord: [-46.698667, -23.624125],
        street: 'Rua Oscar Rodrigues Cajado',
        zipcode: '04710-190'
    },
    borough: 'Santo Amaro',
    cuisine: 'Churrasco',
    grades: [{
        date : ISODate('2019-03-18T22:00:00.000-03:00'),
        grade: 'A',
        score: 99
    },{
        date : ISODate('2019-03-17T22:00:00.000-03:00'),
        grade: 'A',
        score: 101
    }],
    name: 'Rogélio D Monteiro'
},{
    address: {
        building: '158',
        coord: [-46.698077, -23.624637],
        street: 'Rua Oscar Rodrigues Cajado',
        zipcode: '04710-190'
    },
    borough: 'Santo Amaro',
    cuisine: 'Churrasco',
    grades: [{
        date : ISODate('2019-03-18T22:00:00.000-03:00'),
        grade: 'A',
        score: 77
    },{
        date : ISODate('2019-03-17T22:00:00.000-03:00'),
        grade: 'A',
        score: 8
    }],
    name: 'Restaurante Aconchego'
}]);

db.restaurants.find({name: 'Restaurante Aconchego'});
db.restaurants.remove({name: 'Recanto Gaúcho'});

db.restaurants.updateOne({_id: ObjectId('5c8ffeaf68353206bc0c2fd0')}, {
    $set: {'cuisine': 'Italiano'}
});